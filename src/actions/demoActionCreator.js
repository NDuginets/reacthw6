let nextTodoId = 2;

export function addTodo(text) {
  return { type: 'ADD', id: nextTodoId++, text }
}

export function changeStatus(index) {
  return { type: 'CHANGE_STATUS', index  }
}

export function setVisibilityFilter(filter) {
  return { type: 'SET_VISIBILITY_FILTER', filter }
}

export function delTodo(index) {
  return { type: 'DEL', index  }
}


const ActionCreator = {

  fetchPro: () => {
    return function (dispatch, getState) {
      const url = "http://www.json-generator.com/api/json/get/cfJgSoaajS?indent=2";
      dispatch({
        type: 'PROMISE',
        actions: [
          'REQUEST',
          'RESPONSE',
          'ERROR'
        ],
        promise: fetch(url).then( res => res.json() )
      });
    };
  }

}

/*const demoActionCreator = {
  doSomsng: (l;;l) => {
  return function (dispatch, getState) {
    dispatch({
        type: 'ACTION_TYPE',
        payload: ['some', 'data']
      });
  };
}

};*/





export default ActionCreator;
