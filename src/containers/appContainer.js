import ListArtist from '../components/ListArtis';
import { connect } from 'react-redux'
import ActionCreator from '../actions/demoActionCreator';

const mapStateToProps = (state, ownProps) => {
  return {
      data: state.dataFetch
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    ActionCreator: () => {
      dispatch( ActionCreator.fetchPro() );
    }
  };
};

const MyListArtist = connect(
  mapStateToProps,
  mapDispatchToProps
)(ListArtist);


export default MyListArtist;
