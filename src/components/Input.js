import React, { Component } from 'react';
import {addTodo} from '../actions/demoActionCreator'
import { connect } from 'react-redux'



let Input = ({dispatch}) => {

let input;
  return(
    <div>
    <input ref={node => {
        input = node;
    }}/>
      <button onClick={()=> {
          dispatch(addTodo(input.value))
          input.value = '';
      }}>
      Add
      </button>
    </div>
  );

};

Input = connect()(Input)

export default Input;