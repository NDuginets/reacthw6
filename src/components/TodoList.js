import React, { Component } from 'react';
import Todo from './Todo.js'



class TodoList extends Component {
render () {
  return(
    <div>
      {this.props.todolist.map( item => <Todo key={item.id} todo={item}/> )}
    </div>
  );
 }
};

export default TodoList;