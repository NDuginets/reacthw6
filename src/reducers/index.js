import { combineReducers } from 'redux';


const todoInitialState = {
  todolist: [
    {
      text: 'My try',
      id: 0,
      completed: false
    },
    {
      text: 'My cry',
      id: 1,
      completed: false
    }

  ]
}

const dataInitialState = {
  loading: false,
  loaded: false,
  data: [],
  errors: []

}
function dataFetch(state = dataInitialState, action){
  switch( action.type ){
    case "REQUEST":
      return {
              ...state,
              loading:true
            }
    case "RESPONSE":
      return {
              ...state,
              data: action.data,
              loading:false,
              loaded: true,
            }
    case "ERROR":
      return {
              ...state,
              errors:action.error
            }

   /* case "ADD":
      return Object.assign({}, state, {
        todolist: [
          ...state.todolist,
          {
            text: action.text,
            completed: false
          }
        ]
      })*/

   /* case CHANGE_STATUS:
    return Object.assign({}, state, {
      todolist: state.todolist.map((todolist, index) => {
        if(index === action.index) {
          return Object.assign({}, todolist, {
            completed: !todolist.completed
          })
        }
        return todo
      })
    })*/


    default:
      return state;
  }
}


const reducer = combineReducers({
  dataFetch
});

export default reducer;
